# Require otel-ruby
require 'opentelemetry/sdk'

# Export traces to console by default
# ENV['OTEL_TRACES_EXPORTER'] ||= 'console'

# Allow setting the host from the ENV

# configure SDK with defaults
OpenTelemetry::SDK.configure

# Configure tracer
tracer = OpenTelemetry.tracer_provider.tracer('faraday', '1.0')

# create a span
tracer.in_span('foo') do |span|
  # set an attribute
  span.set_attribute('platform', 'osx')
  # add an event
  span.add_event('event in bar')
  # create bar as child of foo
  tracer.in_span('bar') do |child_span|
    # inspect the span
    pp child_span
  end
end